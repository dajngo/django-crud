from django.db import models

# Create your models here.
class Album(models.Model):
    artist = models.CharField(max_length=50)
    album = models.CharField(max_length=50)
    gener = models.CharField(max_length=50)
    logo = models.CharField(max_length=50)

class Songs(models.Model):
    album = models.ForeignKey(Album,on_delete=models.CASCADE)
    file_type = models.CharField(max_length=100)
    song_title = models.CharField(max_length=100)

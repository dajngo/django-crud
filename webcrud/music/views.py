from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from .models import Album
# Create your views here.
def index(request):
    html = ''
    all_album = Album.objects.all()
    template = loader.get_template('music/index.html')
    context = {
        'all_albums': all_album,
    }
    # return render(request, 'music/index.html', context)
    # for album in all_album:
    #     url = '/music/'+str(album.id)+'/'
    #     html += '<a href="'+ url +'">' + album.artist+ '</a><br>'
    return HttpResponse(template.render(context, request))

def detail(request,album_id):
    all_data = Album.objects.get(pk=album_id)
    context = {
        'all_data': all_data,
    }
    return render(request, 'music/details.html', context)

    # return HttpResponse('<h1> All details  '+str(album_id)+'</h1>')
